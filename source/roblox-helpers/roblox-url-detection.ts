// Roblox doesn't make the error code obvious
export const is404 = (): boolean => document.querySelector<HTMLMetaElement>('[rel="canonical"]')!.getAttribute('href')!.includes('?code=404');

export const is500 = (): boolean => document.querySelector<HTMLMetaElement>('[rel="canonical"]')!.getAttribute('href')!.includes('?code=500');

export const isLogin = (): boolean => document.querySelector<HTMLMetaElement>('[rel="canonical"]')!.getAttribute('href')!.includes('/NewLogin');

export const isGame = (url: URL | HTMLAnchorElement | Location = location): boolean => getCleanPathname(url).startsWith('games');

export const isUser = (url: URL | HTMLAnchorElement | Location = location): boolean => getCleanPathname(url).startsWith('users');

export const isCatalog = (url: URL | HTMLAnchorElement | Location = location): boolean => getCleanPathname(url).startsWith('catalog');

export const isGroup = (url: URL | HTMLAnchorElement | Location = location): boolean => getCleanPathname(url).startsWith('groups');

export const hasAvatarCards = (url: URL | HTMLAnchorElement | Location = location): boolean =>
	isGroupConfigureMembers(url);

export const isGroupConfigure = (url: URL | HTMLAnchorElement | Location = location): boolean => getCleanPathname(url) === 'groups/configure';

// We employ location.hash to catch the hash in the URL
export const isGroupConfigureMembers = (url: URL | HTMLAnchorElement | Location = location): boolean => isGroupConfigure(url) && location.hash === '#!/members';

const getUsername = (): string | undefined => document.querySelector('meta[name="user-data"]')?.getAttribute('data-name')!;

const getCleanPathname = (url: URL | HTMLAnchorElement | Location = location): string => url.pathname.replace(/\/+/g, '/').slice(1, url.pathname.endsWith('/') ? -1 : undefined);

const getURL = (url?: URL | HTMLAnchorElement | Location | string): URL | undefined => {
	if (!url) {
		const canonical = document.querySelector<HTMLMetaElement>('[rel="canonical"]')!.getAttribute('href');
		if (canonical) {
			const canonicalUrl = new URL(canonical, location.origin);
			if (getCleanPathname(canonicalUrl).toLowerCase() === getCleanPathname(location).toLowerCase()) {
				return canonicalUrl;
			}
		}
	}

	if (typeof url === 'string') {
		return new URL(url, location.origin);
	}

	return undefined;
};

export const utils = {
	getUsername,
	getCleanPathname,
	getURL,
};
