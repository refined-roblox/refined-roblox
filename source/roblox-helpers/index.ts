export function shouldFeatureRun({
	asLongAs = [() => true],

	include = [() => true],

	exclude = [() => false],
}): boolean {
	return asLongAs.every(c => c()) && include.some(c => c()) && exclude.every(c => !c());
}
