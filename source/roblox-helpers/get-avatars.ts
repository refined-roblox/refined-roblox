type bustSizes = 48 | 50 | 60 | 75 | 100 | 150 | 180 | 352 | 420;
type headshotSizes = bustSizes | 110 | 720;

export async function getUserBust(userId: number,	size: bustSizes): Promise<string | void> {
	const bustInfo = await fetch(`https://thumbnails.roblox.com/v1/users/avatar-bust?userIds=${userId}&size=${size}x${size}&format=Png`);
	const infoJSON = await bustInfo.json();

	return infoJSON.data[0].imageUrl;
}

export async function getUserHeadshot(userId: number,	size: headshotSizes): Promise<string | void> {
	const headshotInfo = await fetch(`https://thumbnails.roblox.com/v1/users/avatar-headshot?userIds=${userId}&size=${size}x${size}&format=Png`);
	const infoJSON = await headshotInfo.json();

	return infoJSON.data[0].imageUrl;
}

export async function getGroupIcon(groupId: number, size: 150 | 420): Promise<string | void> {
	const iconInfo = await fetch(`https://thumbnails.roblox.com/v1/groups/icons?groupIds=${groupId}&size=${size}x${size}&format=Png`);
	const infoJSON = await iconInfo.json();

	return infoJSON.data[0].imageUrl;
}
