import 'webext-base-css/webext-base.css';
import './options.css';
import React from 'dom-chef';
import cache from 'webext-storage-cache';
import domify from 'doma';
import select from 'select-dom';
import delegate from 'delegate-it';
import fitTextarea from 'fit-textarea';
import * as indentTextarea from 'indent-textarea';

import featureLink from './helpers/feature-link';
import clearCacheHandler from './helpers/clear-cache-handler';
import {getLocalHotfixes} from './helpers/hotfix';
import {createRbxIssueLink} from './helpers/rbx-issue-link';
import {importedFeatures, featuresMeta} from '../readme.md';
import {perDomainOptions, renamedFeatures} from './options-storage';

function moveNewAndDisabledFeaturesToTop(): void {
	const container = select('.js-features')!;

	for (const unchecked of select.all('.feature-checkbox:not(:checked)', container).reverse()) {
		container.prepend(unchecked.closest('.feature')!);
	}

	for (const newFeature of select.all('.feature-new', container).reverse()) {
		container.prepend(newFeature);
	}
}

function buildFeatureCheckbox({id, description, screenshot}: FeatureMeta): HTMLElement {
	const descriptionElement = domify.one(description)!;
	descriptionElement.className = 'description';

	return (
		<div className="feature" data-text={`${id} ${description}`.toLowerCase()}>
			<input type="checkbox" name={`feature:${id}`} id={id} className="feature-checkbox"/>
			<div className="info">
				<label className="feature-name" htmlFor={id}>{id}</label>
				{' '}
				<a href={featureLink(id)} className="feature-link">
					source
				</a>
				<input hidden type="checkbox" className="screenshot-toggle"/>
				{screenshot && (
					<a href={screenshot} className="screenshot-link">
						screenshot
					</a>
				)}
				{descriptionElement}
				{screenshot && (
					<img hidden data-src={screenshot} className="screenshot"/>
				)}
			</div>
		</div>
	);
}

async function findFeatureHandler(event: Event): Promise<void> {
	const options = await perDomainOptions.getOptionsForOrigin().getAll();
	const enabledFeatures = importedFeatures.filter(featureId => options['feature:' + featureId]);
	await cache.set<FeatureID[]>('bisect', enabledFeatures, {minutes: 5});

	const button = event.target as HTMLButtonElement;
	button.disabled = true;
	setTimeout(() => {
		button.disabled = false;
	}, 10_000);

	select('#find-feature-message')!.hidden = false;
}

function summaryHandler(event: delegate.Event<MouseEvent>): void {
	if (event.ctrlKey || event.metaKey || event.shiftKey) {
		return;
	}

	event.preventDefault();
	if (event.altKey) {
		for (const screenshotLink of select.all('.screenshot-link')) {
			toggleScreenshot(screenshotLink.parentElement!);
		}
	} else {
		const feature = event.delegateTarget.parentElement!;
		toggleScreenshot(feature);
	}
}

function toggleScreenshot(feature: Element): void {
	const toggle = feature.querySelector('input.screenshot-toggle')!;
	toggle.checked = !toggle.checked;

	const screenshot = feature.querySelector('img.screenshot')!;
	screenshot.src = screenshot.dataset.src!;
}

function featuresFilterHandler(event: Event): void {
	const keywords = (event.currentTarget as HTMLInputElement).value.toLowerCase()
		.replace(/\W/g, ' ')
		.split(/\s+/)
		.filter(Boolean);
	for (const feature of select.all('.feature')) {
		feature.hidden = !keywords.every(word => feature.dataset.text!.includes(word));
	}
}

async function highlightNewFeatures(): Promise<void> {
	const {featuresAlreadySeen} = await browser.storage.local.get({featuresAlreadySeen: {}});
	for (const [from, to] of renamedFeatures) {
		featuresAlreadySeen[to] = featuresAlreadySeen[from];
	}

	const isFirstVisit = Object.keys(featuresAlreadySeen).length === 0;
	const tenDaysAgo = Date.now() - (10 * 24 * 60 * 60 * 1000);
	for (const feature of select.all('.feature-checkbox')) {
		if (!(feature.id in featuresAlreadySeen)) {
			featuresAlreadySeen[feature.id] = isFirstVisit ? tenDaysAgo : Date.now();
		}

		if (featuresAlreadySeen[feature.id] > tenDaysAgo) {
			feature.parentElement!.classList.add('feature-new');
		}
	}

	void browser.storage.local.set({featuresAlreadySeen});
}

async function getLocalHotfixesAsNotice(): Promise<HTMLElement> {
	const disabledFeatures = <div className="js-hotfixes"/>;

	for (const [feature, relatedIssue] of await getLocalHotfixes()) {
		if (importedFeatures.includes(feature)) {
			disabledFeatures.append(
				<p><code>{feature}</code> has been temporarily disabled due to {createRbxIssueLink(relatedIssue)}.</p>,
			);
			const input = select<HTMLInputElement>('#' + feature)!;
			input.disabled = true;
			input.removeAttribute('name');
			select(`.feature-name[for="${feature}"]`)!.after(
				<span className="hotfix-notice"> (Disabled due to {createRbxIssueLink(relatedIssue)})</span>,
			);
		}
	}

	return disabledFeatures;
}

async function generateDom(): Promise<void> {
	select('.js-features')!.append(...featuresMeta
		.filter(feature => importedFeatures.includes(feature.id))
		.map(feature => buildFeatureCheckbox(feature)),
	);

	select('.js-features')!.before(await getLocalHotfixesAsNotice());

	await perDomainOptions.syncForm('form');

	await highlightNewFeatures();
	moveNewAndDisabledFeaturesToTop();

	if (process.env.NODE_ENV === 'development') {
		select('#logHTTP-line')!.hidden = false;
	}

	select('.features-header')!.append(` (${featuresMeta.length})`);
}

function addEventListeners(): void {
	browser.permissions.onRemoved.addListener(() => {
		location.reload();
	});
	browser.permissions.onAdded.addListener(() => {
		location.reload();
	});

	fitTextarea.watch('textarea');
	indentTextarea.watch('textarea');

	delegate(document, '.screenshot-link', 'click', summaryHandler);

	select('#filter-features')!.addEventListener('input', featuresFilterHandler);

	select('#clear-cache')!.addEventListener('click', clearCacheHandler);

	select('#find-feature')!.addEventListener('click', findFeatureHandler);

	delegate(document, 'a[href^="http"]', 'click', event => {
		if (!event.defaultPrevented) {
			event.preventDefault();
			window.open(event.delegateTarget.href);
		}
	});

	select('#show-debugging button')!.addEventListener('click', function () {
		this.parentElement!.remove();
	});
}

async function init(): Promise<void> {
	await generateDom();
	addEventListeners();
}

void init();
