import 'webext-dynamic-content-scripts';
import cache from 'webext-storage-cache';
import addDomainPermissionToggle from 'webext-domain-permission-toggle';

import optionsStorage from './options-storage';

addDomainPermissionToggle();

const messageHandlers = {
	openUrls(urls: string[], {tab}: browser.runtime.MessageSender) {
		for (const [i, url] of urls.entries()) {
			void browser.tabs.create({
				url,
				index: tab!.index + i + 1,
				active: false,
			});
		}
	},
	closeTab(_: any, {tab}: browser.runtime.MessageSender) {
		void browser.tabs.remove(tab!.id!);
	},
	async fetch(url: string) {
		const response = await fetch(url);
		return response.text();
	},
	async fetchJSON(url: string) {
		const response = await fetch(url);
		return response.json();
	},
	openOptionsPage() {
		void browser.runtime.openOptionsPage();
	},
};

browser.runtime.onMessage.addListener((message: typeof messageHandlers, sender) => {
	for (const id of Object.keys(message) as Array<keyof typeof messageHandlers>) {
		if (id in messageHandlers) {
			return messageHandlers[id](message[id], sender);
		}
	}
});

browser.browserAction.onClicked.addListener(async tab => {
	const {actionUrl} = await optionsStorage.getAll();
	void browser.tabs.create({
		openerTabId: tab.id,
		url: actionUrl || 'https://roblox.com',
	});
});

browser.runtime.onInstalled.addListener(async () => {
	await cache.delete('hotfixes');
	await cache.delete('style-hotfixes');
});
