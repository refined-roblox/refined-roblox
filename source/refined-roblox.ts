import './helpers/types.d.ts';

import './refined-roblox.css';

import './features/group-user-join-date';
import './features/hide-low-quality-comments';
import './features/markdown-descriptions';
import './features/user-avatars';
