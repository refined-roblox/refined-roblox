import select from 'select-dom';
import delegate from 'delegate-it';

const cardsWithListeners = new WeakSet();
const handlers = new Set<VoidFunction>();

let cardCount = 0;
const observer = new MutationObserver(() => {
	const cardNewCount = select.all('.avatar-card').length;
	if (cardNewCount > cardCount) {
		cardCount = cardNewCount;
		run();
	}

	cardCount = cardNewCount;
});

function run(): void {
	handlers.forEach(async callback => {
		callback();
	});
}

function headingClickHandler(): void {
	// This is inefficient but I can't see another way to do this
	setTimeout(run, 750);
}

function addListeners(): delegate.Subscription[] {
	const cards = select('.avatar-cards');
	if (!cards || cardsWithListeners.has(cards)) {
		return [];
	}

	cardsWithListeners.add(cards);

	cardCount = select.all('.avatar-card').length;
	observer.observe(cards, {
		childList: true,
	});

	return [
		delegate(document, '.rbx-tab-heading', 'click', headingClickHandler),
	];
}

export default function onNewAvatarCard(callback: VoidFunction): Deinit {
	handlers.add(callback);

	return [
		...addListeners(),
		handlers.clear,
		observer,
	];
}
