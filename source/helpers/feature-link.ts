export default function featureLink(id: FeatureID): string {
	return `https://gitlab.com/refined-roblox/refined-roblox/-/tree/main/source/features/${id}.tsx`;
}
