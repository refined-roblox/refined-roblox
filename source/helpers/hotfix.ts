import cache from 'webext-storage-cache';
import compareVersions from 'tiny-version-compare';

import {RBXOptions} from '../options-storage';
import isDevelopmentVersion from './is-development-version';
import {concatenateTemplateLiteralTag} from './template-literal';

function parseCsv(content: string): string[][] {
	const lines = [];
	const [_header, ...rawLines] = content.trim().split('\n');
	for (const line of rawLines) {
		if (line.trim()) {
			lines.push(line.split(',').map(cell => cell.trim()));
		}
	}

	return lines;
}

async function fetchHotfix(path: string): Promise<string> {
	const request = await fetch(`https://gitlab.com/api/v4/projects/37591518/repository/files/${path}?ref=main`);
	const {content} = await request.json();

	if (content) {
		return atob(content).trim();
	}

	return '';
}

export type HotfixStorage = Array<[FeatureID, string]>;

export const updateHotfixes = cache.function(async (version: string): Promise<HotfixStorage> => {
	const content = await fetchHotfix('broken-features.csv');
	if (!content) {
		return [];
	}

	const storage: HotfixStorage = [];
	for (const [featureID, relatedIssue, unaffectedVersion] of parseCsv(content)) {
		if (featureID && relatedIssue && (!unaffectedVersion || compareVersions(unaffectedVersion, version) > 0)) {
			storage.push([featureID as FeatureID, relatedIssue]);
		}
	}

	return storage;
}, {
	maxAge: {hours: 6},
	staleWhileRevalidate: {days: 30},
	cacheKey: () => 'hotfixes',
});

export const updateStyleHotfixes = cache.function(
	async (version: string): Promise<string> => fetchHotfix(`style/${version}.css`),
	{
		maxAge: {hours: 6},
		staleWhileRevalidate: {days: 30},
		cacheKey: () => 'style-hotfixes',
	},
);

export async function getLocalHotfixes(): Promise<HotfixStorage> {
	if (isDevelopmentVersion()) {
		return [];
	}

	return await cache.get<HotfixStorage>('hotfixes') ?? [];
}

export async function getLocalHotfixesAsOptions(): Promise<Partial<RBXOptions>> {
	const options: Partial<RBXOptions> = {};
	for (const [feature] of await getLocalHotfixes()) {
		options[`feature:${feature}`] = false;
	}

	return options;
}

export async function getStyleHotfixes(): Promise<string> {
	if (isDevelopmentVersion()) {
		return '';
	}

	return await cache.get<string>('style-hotfixes') ?? '';
}

const stringHotfixesKey = 'strings-hotfixes';
let localStrings: Record<string, string> = {};
export function _(...arguments_: Parameters<typeof concatenateTemplateLiteralTag>): string {
	const original = concatenateTemplateLiteralTag(...arguments_);
	return localStrings[original] ?? original;
}

export async function getLocalStrings(): Promise<void> {
	if (isDevelopmentVersion()) {
		return;
	}

	localStrings = await cache.get<Record<string, string>>(stringHotfixesKey) ?? {};
}

export const updateLocalStrings = cache.function(async (): Promise<Record<string, string>> => {
	const json = await fetchHotfix('strings.json');
	return json ? JSON.parse(json) : {};
}, {
	maxAge: {hours: 6},
	staleWhileRevalidate: {days: 30},
	cacheKey: () => stringHotfixesKey,
});
