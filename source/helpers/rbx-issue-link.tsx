import React from 'dom-chef';

export function createRbxIssueLink(issueNumber: number | string): Element {
	const issueUrl = getRbxIssueUrl(issueNumber);
	return (
		<a target="_blank" rel="noopener noreferrer" href={issueUrl}>
			#{issueNumber}
		</a>
	);
}

export function getRbxIssueUrl(issueNumber: number | string): string {
	return `https://gitlab.com/refined-roblox/refined-roblox/-/issues/${issueNumber}`;
}
