export function concatenateTemplateLiteralTag(
	strings: TemplateStringsArray,
	...keys: string[]
): string {
	return strings
		.map((string, i) => string + (i < keys.length ? keys[i] : ''))
		.join('');
}

export const html = concatenateTemplateLiteralTag;
export const css = concatenateTemplateLiteralTag;
