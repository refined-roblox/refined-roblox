import React from 'dom-chef';
import cache from 'webext-storage-cache';
import select from 'select-dom';
import elementReady from 'element-ready';

import pluralize from './pluralize';
import featureLink from './feature-link';
import {importedFeatures} from '../../readme.md';

const getMiddleStep = (list: any[]): number => Math.floor(list.length / 2);

async function onChoiceButtonClick({currentTarget: button}: React.MouseEvent<HTMLButtonElement>): Promise<void> {
	const answer = button.value;
	const bisectedFeatures = (await cache.get<FeatureID[]>('bisect'))!;

	if (bisectedFeatures.length > 1) {
		await cache.set('bisect', answer === 'yes'
			? bisectedFeatures.slice(0, getMiddleStep(bisectedFeatures))
			: bisectedFeatures.slice(getMiddleStep(bisectedFeatures)),
		);

		button.parentElement!.replaceWith(<div className="btn-cta-md">Reloading…</div>);
		button.parentElement!.setAttribute('disabled', '');
		location.reload();
		return;
	}

	if (answer === 'yes') {
		createMessageBox('No features were enabled on this page. Try disabling Refined Roblox to see if it belongs to it at all.');
	} else {
		const feature = (
			<a href={featureLink(bisectedFeatures[0])}>
				<code>{bisectedFeatures[0]}</code>
			</a>
		);

		createMessageBox(<>The change or issue is caused by {feature}.</>);
	}

	await cache.delete('bisect');
	window.removeEventListener('visibilitychange', hideMessage);
}

async function onEndButtonClick(): Promise<void> {
	await cache.delete('bisect');
	location.reload();
}

function createMessageBox(message: Element | string, extraButtons?: Element): void {
	select('#rbx-bisect-dialog')?.remove();
	document.body.append(
		<div className="modal-dialog ">
			<div id="rbx-bisect-dialog" className="modal-content">
				<div className="modal-body ng-scope">
					<p className="body-text text-description ng-binding ng-scope">
						{message}
					</p>
				</div>
				<div className="modal-buttons">
					<div className="navbar-left">
						<button type="button" className="btn-secondary-md" onClick={onEndButtonClick}>Exit</button>
					</div>
					{extraButtons}
				</div>
			</div>
		</div>,
	);
}

async function hideMessage(): Promise<void> {
	if (!await cache.get<FeatureID[]>('bisect')) {
		createMessageBox('Process completed in another tab');
	}
}

export default async function bisectFeatures(): Promise<Record<string, boolean> | void> {
	const bisectedFeatures = await cache.get<FeatureID[]>('bisect');
	if (!bisectedFeatures) {
		return;
	}

	console.log(`Bisecting ${bisectedFeatures.length} features:\n${bisectedFeatures.join('\n')}`);

	const steps = Math.ceil(Math.log2(Math.max(bisectedFeatures.length))) + 1;
	await elementReady('body');
	createMessageBox(
		`Do you see the change or issue? (${pluralize(steps, 'last step', '$$ steps remaining')})`,
		<div className="navbar-right">
			<button type="button" className="rbx-btn-margin btn-alert-md" onClick={onChoiceButtonClick}>No</button>
			<button type="button" className="btn-growth-md" onClick={onChoiceButtonClick}>Yes</button>
		</div>,
	);

	window.addEventListener('visibilitychange', hideMessage);

	const half = getMiddleStep(bisectedFeatures);
	const temporaryOptions: Record<string, boolean> = {};
	for (const feature of importedFeatures) {
		const index = bisectedFeatures.indexOf(feature);
		temporaryOptions[`feature:${feature}`] = index > -1 && index < half;
	}

	console.log(temporaryOptions);
	return temporaryOptions;
}
