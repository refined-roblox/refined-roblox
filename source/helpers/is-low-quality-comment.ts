export default function isLowQualityComment(text: string): boolean {
	return text.replace(/[\s,.!?👎]+|\/e free|cringe|cool/gui, '') === '';
}
