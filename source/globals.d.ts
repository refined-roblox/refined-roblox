/* eslint-disable @typescript-eslint/consistent-indexed-object-style */

type AnyObject = Record<string, any>;
type DeinitHandle = MutationObserver | ResizeObserver | IntersectionObserver | {destroy: VoidFunction} | {abort: VoidFunction} | VoidFunction;
type Deinit = DeinitHandle | DeinitHandle[];

type FeatureID = string & {feature: true};
interface FeatureMeta {
	id: FeatureID;
	description: string;
	screenshot?: string;
}

interface Window {
	content: GlobalFetch;
}

declare module 'markdown-wasm/dist/markdown.node.js';
declare module 'filter-altered-clicks' {
	export default function filterAlteredClicks<Listener>(handler: Listener): Listener;
}

declare module 'size-plugin';

declare module '*.md' {
	export const importedFeatures: FeatureID[];
	export const featuresMeta: FeatureMeta[];
}

interface GlobalEventHandlersEventMap {
	'details:toggled': CustomEvent;
	'filterable:change': CustomEvent;
	'menu:activated': CustomEvent;
	'pjax:error': CustomEvent;
	'page:loaded': CustomEvent;
	'turbo:visit': CustomEvent;
	'session:resume': CustomEvent;
	'socket:message': CustomEvent;
	'input': InputEvent;
}

declare namespace JSX {
	/* eslint-disable @typescript-eslint/no-redundant-type-constituents */
	interface IntrinsicElements {
		'clipboard-copy': IntrinsicElements.button & {for?: string};
		'details-dialog': IntrinsicElements.div & {tabindex: string};
		'details-menu': IntrinsicElements.div & {src?: string; preload?: boolean};
		'has-rbx': IntrinsicElements.div;
		'has-rbx-inner': IntrinsicElements.div;
		'include-fragment': IntrinsicElements.div & {src?: string};
		'label': IntrinsicElements.label & {for?: string};
		'relative-time': IntrinsicElements.div & {datetime: string};
		'tab-container': IntrinsicElements.div;
		'time-ago': IntrinsicElements.div & {datetime: string; format?: string};
	}
	/* eslint-enable @typescript-eslint/no-redundant-type-constituents */

	type BaseElement = IntrinsicElements['div'];
	interface IntrinsicAttributes extends BaseElement {
		width?: number;
		height?: number;
	}
}

interface NamedNodeMap {
	[key: string]: Attr;
}

interface HTMLFormControlsCollection {
	[key: string]: HTMLInputElement | HTMLTextAreaElement | HTMLButtonElement;
}

declare module 'react' {
	const FC = (): JSX.Element => JSX.Element;
	const React = {FC};
	export default React;
}

interface Node extends EventTarget {
	cloneNode(deep?: boolean): this;
}
