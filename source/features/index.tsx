import React from 'dom-chef';
import select from 'select-dom';
import domLoaded from 'dom-loaded';
import stripIndent from 'strip-indent';
import {Promisable} from 'type-fest';

import * as pageDetect from '../roblox-helpers/roblox-url-detection';
import waitFor from '../helpers/wait-for';
import onNewAvatarCard from '../roblox-events/on-new-avatar-card';
import bisectFeatures from '../helpers/bisect';
import {shouldFeatureRun} from '../roblox-helpers';
import optionsStorage, {RBXOptions} from '../options-storage';
import {
	getLocalHotfixesAsOptions,
	getLocalStrings,
	getStyleHotfixes,
	updateHotfixes,
	updateLocalStrings,
	updateStyleHotfixes,
	_,
} from '../helpers/hotfix';

type BooleanFunction = () => boolean;
export type CallerFunction = (callback: VoidFunction, signal: AbortSignal) => void | Promise<void> | Deinit;
type FeatureInitResult = void | false | Deinit;
type FeatureInit = (signal: AbortSignal) => Promisable<FeatureInitResult>;

interface FeatureLoader extends Partial<InternalRunConfig> {
	/** This only adds the shortcut to the help screen, it doesn't enable it. @default {} */
	shortcuts?: Record<string, string>;

	/** Whether to wait for DOM ready before running `init`. `false` makes `init` run right as soon as `body` is found. @default true */
	awaitDomReady?: false;

	/** When pressing the back button, DOM changes and listeners are still there, so normally `init` isn’t called again thanks to an automatic duplicate detection.
	This detection however might cause problems or not work correctly in some cases #3945, so it can be disabled with `false` or by passing a custom selector to use as duplication check
	@default true */
	deduplicate?: false | string;

	/** When true, don’t run the `init` on page load but only add the `additionalListeners`. @default false */
	onlyAdditionalListeners?: true;

	init: FeatureInit;
}

interface InternalRunConfig {
	asLongAs: BooleanFunction[] | undefined;
	include: BooleanFunction[] | undefined;
	exclude: BooleanFunction[] | undefined;
	init: FeatureInit;
	additionalListeners: CallerFunction[];

	onlyAdditionalListeners: boolean;
}

const {version} = browser.runtime.getManifest();

const logError = (url: string, error: unknown): void => {
	const id = getFeatureID(url);
	const message = error instanceof Error ? error.message : String(error);

	if (message.includes('token')) {
		console.log('ℹ️', id, '→', message);
		return;
	}

	const searchIssueUrl = new URL('https://gitlab.com/refined-roblox/refined-roblox/issues');
	searchIssueUrl.searchParams.set('search', `${message}`);
	searchIssueUrl.searchParams.set('sort', 'updated_desc');
	searchIssueUrl.searchParams.set('state', 'opened');

	const newIssueUrl = new URL('https://gitlab.com/refined-roblox/refined-roblox/-/issues/new');
	newIssueUrl.searchParams.set('issue[title]', `\`${encodeURIComponent(id)}\`%3A ${encodeURIComponent(message)}`);
	newIssueUrl.searchParams.set('issue[description]', [
		'```',
		encodeURIComponent(String(error instanceof Error ? error.stack! : error).trim()),
		'```',
	].join('\n'));

	console.group(`❌ ${id}`);
	console.log(`📕 ${version}`, error);
	console.log('🔍 Search issue', searchIssueUrl.href);
	console.log('🚨 Report issue', newIssueUrl.href);
	console.groupEnd();
};

const log = {
	info: console.log,
	http: console.log,
	error: logError,
};

// eslint-disable-next-line no-async-promise-executor
const globalReady: Promise<RBXOptions> = new Promise(async resolve => {
	const [options, localHotfixes, hotfixCSS, bisectedFeatures] = await Promise.all([
		optionsStorage.getAll(),
		getLocalHotfixesAsOptions(),
		getStyleHotfixes(),
		bisectFeatures(),
		getLocalStrings(),
	]);

	await waitFor(() => document.body);

	if (hotfixCSS.length > 0 || options.customCSS.trim().length > 0) {
		document.body.prepend(
			<style>{hotfixCSS}</style>,
			<style>{options.customCSS}</style>,
		);
	}

	void updateStyleHotfixes(version);
	void updateLocalStrings();

	if (bisectedFeatures) {
		Object.assign(options, bisectedFeatures);
	} else {
		void updateHotfixes(version);
		Object.assign(options, localHotfixes);
	}

	log.info = options.logging ? console.log : () => {/* No logging */};
	log.http = options.logHTTP ? console.log : () => {/* No logging */};

	if (pageDetect.is500() || pageDetect.isLogin()) {
		return;
	}

	if (select.exists('html.refined-roblox')) {
		console.warn(stripIndent(`
			Refined Roblox has been loaded twice. This may be because:

			• You loaded the developer version, or
			• The extension just updated

			If you see this at every load, please open an issue mentioning the browser you're using and the URL where this appears.
		`));
		return;
	}

	if (select.exists('#wrap.logged-out')) {
		console.warn('Refined Roblox is only expected to work when you’re logged in to Roblox. Errors will not be shown.');
		features.log.error = () => {/* No logging */};
	}

	document.documentElement.classList.add('refined-roblox');

	resolve(options);
});

function getDeinitHandler(deinit: DeinitHandle): VoidFunction {
	if (deinit instanceof MutationObserver || deinit instanceof ResizeObserver || deinit instanceof IntersectionObserver) {
		return () => {
			deinit.disconnect();
		};
	}

	if ('abort' in deinit) {
		return () => {
			deinit.abort();
		};
	}

	if ('destroy' in deinit) {
		return deinit.destroy;
	}

	return deinit;
}

function setupDeinit(deinit: Deinit): void {
	const deinitFunctions = Array.isArray(deinit) ? deinit : [deinit];
	for (const deinit of deinitFunctions) {
		document.addEventListener('beforeunload', getDeinitHandler(deinit), {once: true});
	}
}

const setupPageLoad = async (id: FeatureID, config: InternalRunConfig): Promise<void> => {
	const {asLongAs, include, exclude, init, additionalListeners, onlyAdditionalListeners} = config;

	if (!shouldFeatureRun({asLongAs, include, exclude})) {
		return;
	}

	const deinitController = new AbortController();
	document.addEventListener('beforeunload', () => {
		deinitController.abort();
	}, {
		once: true,
	});

	const runFeature = async (): Promise<void> => {
		let result: FeatureInitResult;

		try {
			result = await init(deinitController.signal);
			if (result !== false && !id?.startsWith('rbx')) {
				log.info('✅', id);
			}
		} catch (error: unknown) {
			log.error(id, error);
		}

		if (result) {
			setupDeinit(result);
		}
	};

	if (!onlyAdditionalListeners) {
		await runFeature();
	}

	await domLoaded;
	for (const listener of additionalListeners) {
		const deinit = listener(runFeature, deinitController.signal);
		if (deinit && !(deinit instanceof Promise)) {
			setupDeinit(deinit);
		}
	}
};

const shortcutMap = new Map<string, string>();

const defaultPairs = new Map([
	[pageDetect.hasAvatarCards, onNewAvatarCard],
]);

function enforceDefaults(
	id: FeatureID,
	include: InternalRunConfig['include'],
	additionalListeners: InternalRunConfig['additionalListeners'],
): void {
	for (const [detection, listener] of defaultPairs) {
		if (!include?.includes(detection)) {
			continue;
		}

		if (additionalListeners.includes(listener)) {
			console.error(`❌ ${id} → If you use \`${detection.name}\` you don’t need to specify \`${listener.name}\``);
		} else {
			additionalListeners.push(listener);
		}
	}
}

const getFeatureID = (url: string): FeatureID => url.split('/').pop()!.split('.')[0] as FeatureID;

const add = async (url: string, ...loaders: FeatureLoader[]): Promise<void> => {
	const id = getFeatureID(url);
	const options = await globalReady;
	if (!options[`feature:${id}`] && !id.startsWith('rbx')) {
		log.info('↩️', 'Skipping', id);
		return;
	}

	for (const loader of loaders) {
		const {
			shortcuts = {},
			asLongAs,
			include,
			exclude,
			init,
			awaitDomReady = true,
			deduplicate = 'has-rbx',
			onlyAdditionalListeners = false,
			additionalListeners = [],
		} = loader;

		for (const [hotkey, description] of Object.entries(shortcuts)) {
			shortcutMap.set(hotkey, description);
		}

		if (pageDetect.is404() && !include?.includes(pageDetect.is404) && !asLongAs?.includes(pageDetect.is404)) {
			continue;
		}

		enforceDefaults(id, include, additionalListeners);

		const details = {asLongAs, include, exclude, init, additionalListeners, onlyAdditionalListeners};
		if (awaitDomReady) {
			(async () => {
				await domLoaded;
				await setupPageLoad(id, details);
			})();
		} else {
			void setupPageLoad(id, details);
		}

		document.addEventListener('DOMReady', () => {
			if (!deduplicate || !select.exists(deduplicate)) {
				void setupPageLoad(id, details);
			}
		});
	}
};

const addCssFeature = async (url: string, include: BooleanFunction[] | undefined): Promise<void> => {
	const id = getFeatureID(url);
	void add(id, {
		include,
		deduplicate: false,
		awaitDomReady: false,
		init() {
			document.documentElement.classList.add('rbx-' + id);
		},
	});
};

void add('rbx-deduplicator' as FeatureID, {
	deduplicate: false,
	async init() {
		await Promise.resolve();
		select(_`ng-class`)?.append(<has-rbx-inner/>);
	},
});

const features = {
	add,
	addCssFeature,
	log,
	shortcutMap,
	getFeatureID,
};

export default features;
