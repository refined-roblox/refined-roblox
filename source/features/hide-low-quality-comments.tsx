import './hide-low-quality-comments.css';
import delay from 'delay';
import React from 'dom-chef';
import select from 'select-dom';
import delegate from 'delegate-it';

import features from '.';
import * as pageDetect from '../roblox-helpers/roblox-url-detection';
import isLowQualityComment from '../helpers/is-low-quality-comment';

// eslint-disable-next-line import/prefer-default-export
export const singleParagraphCommentSelector = '.comment-body > p';

async function unhide(event: delegate.Event): Promise<void> {
	for (const comment of select.all('.rbx-hidden-comment')) {
		comment.classList.remove('rbx-hidden-comment');
		comment.classList.add('rbx-unhidden-comment');
	}

	await delay(10);

	select('.rbx-hidden-comment')!.scrollIntoView();
	event.delegateTarget.parentElement!.remove();
}

function hideComment(comment: HTMLElement): void {
	comment.hidden = true;
	comment.classList.add('rbx-hidden-comment');
}

async function init(): Promise<void | Deinit> {
	let lowQualityCount = 0;
	// TODO: Replace with an actual loading check.
	await delay(1500);

	for (const commentText of select.all(singleParagraphCommentSelector)) {
		console.log(commentText)
		if (commentText.innerHTML.includes('<br>') && !commentText.innerHTML.endsWith('<br>')) {
			continue;
		}

		if (!isLowQualityComment(commentText.textContent!)) {
			continue;
		}

		// we explicitly call getAttribute to 
		const author = commentText.parentElement!.children[0].getAttribute('href')
		if (author === select('.text-label > a')!.href) {
			continue;
		}

		const comment = commentText!.parentElement!.parentElement
		hideComment(comment!);
		lowQualityCount++;
	}

	if (lowQualityCount > 0) {
		select('.comments.vlist')!.prepend(
			<p className="rbx-low-quality-comments-note">
				{`${lowQualityCount} unhelpful comment${lowQualityCount > 1 ? 's were' : ' was'} automatically hidden. `}
				<button className="btn-secondary-md rbx-unhide-low-quality-comments" type="button">Show</button>
			</p>,
		);
		return delegate(document, '.rbx-unhide-low-quality-comments', 'click', unhide);
	}
}

// TODO: Rerun on a new comment
void features.add(import.meta.url, {
	include: [
		pageDetect.isCatalog,
	],
	deduplicate: 'has-rbx-inner',
	init,
});