import React from 'dom-chef';
import select from 'select-dom';

import features from '.';
import * as pageDetect from '../roblox-helpers/roblox-url-detection';
import onNewAvatarCard from '../roblox-events/on-new-avatar-card';

const getUserJoinDate = async (userId: number): Promise<Date> => {
	const userInfo = await fetch(`https://users.roblox.com/v1/users/${userId}`);
	const infoJSON = await userInfo.json();

	return new Date(infoJSON.created);
};

async function init(): Promise<void> {
	const cards = select.all('.avatar-card-container');

	if (select.exists('.rbx-join-date')) {
		return;
	}

	for (const card of cards) {
		if (select.exists('.avatar-card-caption span a', card)) {
			// @ts-expect-error
			const userId = Number.parseInt((/\d+/.exec(select('.avatar-card-caption span a', card)!.href))[0]);
			await getUserJoinDate(userId).then(joinDate => {
				select('.avatar-card-label', card)!.after(
					<div className="rbx-join-date avatar-card-label text-overflow ng-binding ng-scope">{joinDate.toLocaleString().split(',')[0]}</div>,
				);
			});
		}
	}
}

void features.add(import.meta.url, {
	include: [
		pageDetect.isGroupConfigureMembers,
	],
	additionalListeners: [
		onNewAvatarCard,
	],
	init,
});
