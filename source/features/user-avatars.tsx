import React from 'dom-chef';
import select from 'select-dom';
import elementReady from 'element-ready';

import * as pageDetect from '../roblox-helpers/roblox-url-detection';

import features from '.';
import {getUserHeadshot, getGroupIcon} from '../roblox-helpers/get-avatars';

async function init(): Promise<void> {
	let label;
	label = await (pageDetect.isCatalog(document.location) ? elementReady('.text-label .text-name') : elementReady('.game-creator .text-label'));
	if (!label) {
		return;
	}

	let creator;
	creator = pageDetect.isCatalog(document.location) ? new URL(select('.text-label a')!.getAttribute('href')!) : new URL(select('.game-creator a')!.getAttribute('href')!);
	const id = Number.parseInt(/\d+/.exec(creator.toString())![0]);
	let src;

	if (pageDetect.isGroup(creator)) {
		await getGroupIcon(id, 150).then(res => {
			src = res;
		});
	} else if (pageDetect.isUser(creator)) {
		await getUserHeadshot(id, 150).then(res => {
			src = res;
		});
	} else {
		return;
	}

	let alt;
	alt = pageDetect.isCatalog(document.location) ? select('.text-label a')!.text : select('.game-creator a')!.text;

	const avatar = (
		<img
			id="rbx-avatar"
			src={src}
			width="24"
			height="24"
			alt={`${alt}`}
		/>
	);

	if (pageDetect.isUser(creator)) {
		avatar.classList.add('rbx-user-avatar');
	}

	if (pageDetect.isCatalog(document.location)) {
		avatar.id = 'rbx-catalog-avatar';
		label.before(avatar);
	} else {
		label.after(avatar);
	}
}

void features.add(import.meta.url, {
	include: [
		pageDetect.isGame,
		pageDetect.isCatalog,
	],
	init,
	awaitDomReady: false,
});
