import select from 'select-dom';
import MarkdownIt from 'markdown-it';

import * as pageDetect from '../roblox-helpers/roblox-url-detection';

import features from '.';

function init(): void {
	const description = select('.game-description')!;
	const md = new MarkdownIt({
		breaks: true,
	});

	const markdownDescription = md.render(description.textContent!);
	description.innerHTML = markdownDescription;
}

void features.add(import.meta.url, {
	include: [
		pageDetect.isGame,
	],
	init,
	awaitDomReady: false,
});
