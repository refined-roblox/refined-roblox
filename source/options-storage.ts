import OptionsSyncPerDomain from 'webext-options-sync-per-domain';

import {importedFeatures} from '../readme.md';

export type RBXOptions = typeof defaults;

// eslint-disable-next-line prefer-object-spread
const defaults = Object.assign({
	actionUrl: '',
	customCSS: '',
	logging: false,
	logHTTP: false,
}, Object.fromEntries(importedFeatures.map(id => [`feature:${id}`, true])));

export const renamedFeatures = new Map<string, string>([
	['show-avatars', 'user-avatars'],
]);

export function getNewFeatureName(possibleFeatureName: string): FeatureID | undefined {
	let newFeatureName = possibleFeatureName;
	while (renamedFeatures.has(newFeatureName)) {
		newFeatureName = renamedFeatures.get(newFeatureName)!;
	}

	return importedFeatures.includes(newFeatureName as FeatureID) ? newFeatureName as FeatureID : undefined;
}

const migrations = [
	function (options: RBXOptions): void {
		for (const [from, to] of renamedFeatures) {
			if (typeof options[`feature:${from}`] === 'boolean') {
				options[`feature:${to}`] = options[`feature:${from}`];
			}
		}
	},

	OptionsSyncPerDomain.migrations.removeUnused,
];

export const perDomainOptions = new OptionsSyncPerDomain({defaults, migrations});
export default perDomainOptions.getOptionsForOrigin();
