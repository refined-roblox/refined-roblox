# Refined Roblox

> Browser extension to add various tweaks to the Roblox interface.

## Install

[link-firefox]: https://addons.mozilla.org/firefox/addon/refined-roblox/ 'Version published on Mozilla Add-ons'

[<img src="https://raw.githubusercontent.com/alrra/browser-logos/6e3e6a8/src/firefox/firefox.svg" width="48" alt="Firefox" valign="middle">][link-firefox] [<img valign="middle" src="https://img.shields.io/amo/v/refined-roblox.svg?label=%20">][link-firefox] also available on Firefox Android!

---

### Games

- [](# "user-avatars") Adds an avatar next to the creator of games and assets
- [](# "markdown-descriptions") Renders Markdown in the descriptions of games and assets

### Assets
- [](# "hide-low-quality-comments") Hides low quality comments from catalog items

### Groups

- [](# "group-user-join-date") 🔥 Adds a user's join date to group join requests and member lists.

## Customization

Most features can be disabled in the options of the settings.