[Node v16](https://nodejs.org/en/) is required. To develop:

1. Install Node and npm
2. Run `npm install`
3. Run `npm run build`

The `/distribution` directory will then be created with a compiled version of the extension. Alternatively, run the build script in `/build`.